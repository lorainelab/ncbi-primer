package org.lorainelab.igb.ncbiprimer;

import com.affymetrix.genometry.GenomeVersion;
import com.affymetrix.genometry.GenometryModel;
import com.affymetrix.genometry.event.GenomeVersionSelectionEvent;
import com.affymetrix.genometry.event.GroupSelectionListener;
import com.affymetrix.genometry.event.SeqSelectionEvent;
import com.affymetrix.genometry.event.SeqSelectionListener;
import com.affymetrix.genometry.event.SymSelectionEvent;
import com.affymetrix.genometry.event.SymSelectionListener;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import org.lorainelab.igb.services.IgbService;
import org.lorainelab.igb.services.window.tabs.IgbTabPanel;
import org.lorainelab.igb.services.window.tabs.IgbTabPanelI;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(name = NCBIPrimerBlastOpts.COMPONENT_NAME, service = {IgbTabPanelI.class,GroupSelectionListener.class}, immediate = true)
public class NCBIPrimerBlastOpts extends IgbTabPanel implements GroupSelectionListener, SeqSelectionListener, SymSelectionListener {

    public static final String COMPONENT_NAME = "PE";
    private static final long serialVersionUID = 1L;
    private static final int TAB_POSITION = 8;
    private static NCBIPrimerBlastView ncbiPrimerPane = new NCBIPrimerBlastView();
    private static NCBIPrimerBlastModel primerBlastOptions = new NCBIPrimerBlastModel();
    private static final Logger logger = LoggerFactory.getLogger(NCBIPrimerBlastOpts.class);

    private static IgbService igbService;
     private static GenometryModel gmodel = GenometryModel.getInstance();

    public NCBIPrimerBlastOpts() {
        super("Primer Options", "Primer Options", null, false, 8);
        init();
    }

    @Reference
    public void setIgbService(IgbService igbService) {

        this.igbService = igbService;
        PrimerSearchAction.igbService = igbService;
    }

    public static NCBIPrimerBlastModel getOptions() {
        setBlastOptionsFromGui(primerBlastOptions);
        return primerBlastOptions;
    }

    public int getWeight() {
        return TAB_POSITION;
    }

    private void init() {
        this.setLayout(new BorderLayout());
        this.add(ncbiPrimerPane, BorderLayout.CENTER);
        this.pack();
        setDefaultOptions();
          gmodel.addGroupSelectionListener(this);
          gmodel.addSeqSelectionListener(this);
          setMenuItem();
    }

    public static void setDefaultOptions() {
        primerBlastOptions.setDefaultOptions();
        setGuiOptions(primerBlastOptions);    
       setOrganismName();
    }

    /*
    * Sets GUI from the Default options in PrimeBlastOptions Bean on Installation and clicking reset to default button.
     */
    public static void setGuiOptions(NCBIPrimerBlastModel primerBlastOptions) {

        /*
         *Set Primer Parameters
         */
        JTextField forwardPrimer = ncbiPrimerPane.getForwardPrimerField();
        forwardPrimer.setText(primerBlastOptions.getPrimer5Start());
        ncbiPrimerPane.setForwardPrimerField(forwardPrimer);

        JTextField reversePrimer = ncbiPrimerPane.getReversePrimerField();
        reversePrimer.setText(primerBlastOptions.getPrimer3End());
        ncbiPrimerPane.setReversePrimerField(reversePrimer);

        JTextField minPcrSizeField = ncbiPrimerPane.getMinPcrSizeField();
        minPcrSizeField.setText(primerBlastOptions.getPrimerProductMin().toString());
        ncbiPrimerPane.setMinPcrSizeField(minPcrSizeField);

        JTextField maxPcrSizeField = ncbiPrimerPane.getMaxPcrSizeField();
        maxPcrSizeField.setText(primerBlastOptions.getPrimerProductMax().toString());
        ncbiPrimerPane.setMaxPcrSizeField(maxPcrSizeField);

        JTextField numPrimersField = ncbiPrimerPane.getNumPrimersField();
        numPrimersField.setText(primerBlastOptions.getPrimerNumReturn().toString());
        ncbiPrimerPane.setNumPrimersField(numPrimersField);

        JTextField minTmField = ncbiPrimerPane.getMinTmField();
        minTmField.setText(primerBlastOptions.getPrimerMinTm().toString());
        ncbiPrimerPane.setMinTmField(minTmField);

        JTextField optTmField = ncbiPrimerPane.getOptTmField();
        optTmField.setText(primerBlastOptions.getPrimerOptTm().toString());
        ncbiPrimerPane.setOptTmField(optTmField);

        JTextField maxTmField = ncbiPrimerPane.getMaxTmField();
        maxTmField.setText(primerBlastOptions.getPrimerMaxTm().toString());
        ncbiPrimerPane.setMaxTmField(maxTmField);

        JTextField maxTmDiffField = ncbiPrimerPane.getMaxTmDiffField();
        maxTmDiffField.setText(primerBlastOptions.getPrimerMaxDiffTm().toString());
        ncbiPrimerPane.setMaxTmDiffField(maxTmDiffField);

        /*
         *Set  Primer Pair Specificity Checking Parameters
         */
        JCheckBox searchSpecific = ncbiPrimerPane.getSearchSpecificPrimerCheckbox();
        searchSpecific.setSelected(primerBlastOptions.isSearchSpecificPrimer());
        ncbiPrimerPane.setSearchSpecificPrimerCheckbox(searchSpecific);

        JTextField organismField = ncbiPrimerPane.getOrganismField();
        organismField.setText(primerBlastOptions.getOrganism());
        ncbiPrimerPane.setOrganismField(organismField);

//        String[] database = new String[NCBIPrimerBlastModel.Database.values().length];
//        for (int i = 0; i < NCBIPrimerBlastModel.Database.values().length; i++) {
//            database[i] = NCBIPrimerBlastModel.Database.values()[i].toString();
//        }
        DefaultComboBoxModel model = new DefaultComboBoxModel(NCBIPrimerBlastModel.Database.values());
        JComboBox<String> databaseComboBox = ncbiPrimerPane.getDatabaseComboBox();
        databaseComboBox.setModel(model);
        ncbiPrimerPane.setDatabaseComboBox(databaseComboBox);
       

        JComboBox<String> totalPrimer = ncbiPrimerPane.getTotalPrimerSpecificityMismatchComboBox();
        totalPrimer.setSelectedIndex(1);
      //  totalPrimer.setSelectedItem(primerBlastOptions.getTotalPrimerSpecificityMismatch());
        ncbiPrimerPane.setTotalPrimerSpecificityMismatchComboBox(totalPrimer);

        JComboBox<String> mismatchLength = ncbiPrimerPane.getMismatchRegionLengthComboBox();
        mismatchLength.setSelectedIndex(1);
       // mismatchLength.setSelectedItem(primerBlastOptions.getMismatchRegionLength());
        ncbiPrimerPane.setMismatchRegionLengthComboBox(mismatchLength);

        JComboBox<String> sepecificityMismatch = ncbiPrimerPane.getPrimer3endSpecificityMismatchComboBox();
        sepecificityMismatch.setSelectedIndex(1);
        //sepecificityMismatch.setSelectedItem(primerBlastOptions.getPrimer3endSpecificityMismatch());
        ncbiPrimerPane.setPrimer3endSpecificityMismatchComboBox(sepecificityMismatch);

        JTextField productSizeDeviationField = ncbiPrimerPane.getProductSizeDeviationField();
        productSizeDeviationField.setText(primerBlastOptions.getProductSizeDeviation().toString());
        ncbiPrimerPane.setProductSizeDeviationField(productSizeDeviationField);

        /*
         *Set Post Processing Options
         */
        JCheckBox removePairsExons = ncbiPrimerPane.getRemovePairsNotInExonsCheckBox();
        removePairsExons.setSelected(primerBlastOptions.isRemovePairsNotInExons());
        ncbiPrimerPane.setRemovePairsNotInExonsCheckBox(removePairsExons);
        
        JTextField max3ExonMatch = ncbiPrimerPane.getExonJunctionMatchMax3();
        max3ExonMatch.setText(primerBlastOptions.getExonJunctionMatchMax3().toString());
        ncbiPrimerPane.setExonJunctionMatchMax3(max3ExonMatch);
        
        JTextField min3ExonMatch = ncbiPrimerPane.getExonJunctionMatchMin3();
        min3ExonMatch.setText(primerBlastOptions.getExonJunctionMatchMin3().toString());
        ncbiPrimerPane.setExonJunctionMatchMin3(min3ExonMatch);
        
        JTextField min5ExonMatch = ncbiPrimerPane.getExonJunctionMatchMin5();
        min5ExonMatch.setText(primerBlastOptions.getExonJunctionMatchMin5().toString());
        ncbiPrimerPane.setExonJunctionMatchMin5(min5ExonMatch);
        
        JTextField intronMaxLength = ncbiPrimerPane.getIntronMaxLengthRange();
        intronMaxLength.setText(primerBlastOptions.getIntronMaxLengthRange().toString());
        ncbiPrimerPane.setIntronMaxLengthRange(intronMaxLength);
        
        JTextField intronMinLength = ncbiPrimerPane.getIntronMinLengthRange();
        intronMinLength.setText(primerBlastOptions.getIntronMinLengthRange().toString());
        ncbiPrimerPane.setIntronMinLengthRange(intronMinLength);
        
        JCheckBox intronInclusionCheckBox = ncbiPrimerPane.getIntronInclusionCheckBox();
        intronInclusionCheckBox.setSelected(false);
        ncbiPrimerPane.setIntronInclusionCheckBox(intronInclusionCheckBox);
        
        JComboBox exonJuncComboBox = ncbiPrimerPane.getExonJunctionSpanComboBox();
        exonJuncComboBox.setSelectedIndex(0);
        ncbiPrimerPane.setExonJunctionSpanComboBox(exonJuncComboBox);
    }

    /*
    * Set User Entered options from GUI to Primer Blast Options to run analysis
     */
    public static void setBlastOptionsFromGui(NCBIPrimerBlastModel opts) {
        /*
         *Set Primer Parameters
         */
        if (ncbiPrimerPane.getForwardPrimerField().getText().length() > 0) {
            opts.setPrimer5Start(ncbiPrimerPane.getForwardPrimerField().getText());
        }else{
            opts.setPrimer5Start("");
        }

        if (ncbiPrimerPane.getReversePrimerField().getText().length() > 0) {
            opts.setPrimer3End(ncbiPrimerPane.getReversePrimerField().getText());
        }else{
             opts.setPrimer3End("");
        }
        
        if (ncbiPrimerPane.getMinPcrSizeField().getText().length() > 0) {
            opts.setPrimerProductMin(Integer.parseInt(ncbiPrimerPane.getMinPcrSizeField().getText()));
        }
        if (ncbiPrimerPane.getMaxPcrSizeField().getText().length() > 0) {
            opts.setPrimerProductMax(Integer.parseInt(ncbiPrimerPane.getMaxPcrSizeField().getText()));
        }
        if (ncbiPrimerPane.getNumPrimersField().getText().length() > 0) {
            opts.setPrimerNumReturn(Integer.parseInt(ncbiPrimerPane.getNumPrimersField().getText()));
        }
        if (ncbiPrimerPane.getMinTmField().getText().length() > 0) {
            opts.setPrimerMinTm(Double.parseDouble(ncbiPrimerPane.getMinTmField().getText()));
        }
        if (ncbiPrimerPane.getOptTmField().getText().length() > 0) {
            opts.setPrimerOptTm(Double.parseDouble(ncbiPrimerPane.getOptTmField().getText()));
        }
        if (ncbiPrimerPane.getMaxTmField().getText().length() > 0) {
            opts.setPrimerMaxTm(Double.parseDouble(ncbiPrimerPane.getMaxTmField().getText()));
        }
        if (ncbiPrimerPane.getMaxTmDiffField().getText().length() > 0) {
            opts.setPrimerMaxDiffTm(Double.parseDouble(ncbiPrimerPane.getMaxTmDiffField().getText()));
        }

        /*
         *Set  Primer Pair Specificity Checking Parameters
         */
        opts.setSearchSpecificPrimer(ncbiPrimerPane.getSearchSpecificPrimerCheckbox().isSelected());
        if (ncbiPrimerPane.getOrganismField().getText().length() > 0) {
            opts.setOrganism(ncbiPrimerPane.getOrganismField().getText());
        }
           opts.setPrimerSpecificityDatabase((NCBIPrimerBlastModel.Database) ncbiPrimerPane.getDatabaseComboBox().getSelectedItem());
       // opts.setPrimerSpecificityDatabase(NCBIPrimerBlastModel.Database.refseq_rna);

        opts.setTotalPrimerSpecificityMismatch(Integer.parseInt(ncbiPrimerPane.getTotalPrimerSpecificityMismatchComboBox().getSelectedItem().toString()));
        opts.setPrimer3endSpecificityMismatch(Integer.parseInt(ncbiPrimerPane.getPrimer3endSpecificityMismatchComboBox().getSelectedItem().toString()));
        opts.setMismatchRegionLength(Integer.parseInt(ncbiPrimerPane.getMismatchRegionLengthComboBox().getSelectedItem().toString()));

        if (ncbiPrimerPane.getProductSizeDeviationField().getText().length() > 0) {
            opts.setProductSizeDeviation(Integer.parseInt(ncbiPrimerPane.getProductSizeDeviationField().getText()));
        }

        /*
         *Set Post Processing Options
         */
        opts.setRemovePairsNotInExons(ncbiPrimerPane.getRemovePairsNotInExonsCheckBox().isSelected());
        
        if(ncbiPrimerPane.getExonJunctionMatchMax3().getText().length() > 0) {
            opts.setExonJunctionMatchMax3(Integer.parseInt(ncbiPrimerPane.getExonJunctionMatchMax3().getText()));
        }
        
        if(ncbiPrimerPane.getExonJunctionMatchMin3().getText().length() > 0) {
            opts.setExonJunctionMatchMin3(Integer.parseInt(ncbiPrimerPane.getExonJunctionMatchMin3().getText()));
        }
        
        if(ncbiPrimerPane.getExonJunctionMatchMin5().getText().length() > 0) {
            opts.setExonJunctionMatchMin5(Integer.parseInt(ncbiPrimerPane.getExonJunctionMatchMin5().getText()));
        }
        
        opts.setExonJunctionSpan(ncbiPrimerPane.getExonJunctionSpanComboBox().getSelectedIndex());
        
        if(ncbiPrimerPane.getIntronMaxLengthRange().getText().length() > 0) {
            opts.setIntronMaxLengthRange(Integer.parseInt(ncbiPrimerPane.getIntronMaxLengthRange().getText()));
        }
        
        if(ncbiPrimerPane.getIntronMinLengthRange().getText().length() > 0) {
            opts.setIntronMinLengthRange(Integer.parseInt(ncbiPrimerPane.getIntronMinLengthRange().getText()));
        }
        
        opts.setIntronInclusion(ncbiPrimerPane.getIntronInclusionCheckBox().isSelected());
    }

    public static void setMenuItem() {

        ncbiPrimerPane.getDatabaseComboBox().getSelectedItem().toString();
        NCBIPrimerPopupListener.PRIMER_MENU_ITEM_TITLE = "Primer Blast " + ncbiPrimerPane.getDatabaseComboBox().getSelectedItem().toString();  
    }

      @Override
    public void groupSelectionChanged(GenomeVersionSelectionEvent evt) {
          setOrganismName();
     
    }
    
      @Override
    public void seqSelectionChanged(SeqSelectionEvent evt) {
              setOrganismName();
    }
    
      @Override
    public void symSelectionChanged(SymSelectionEvent evt) {
              setOrganismName();
    }
    
    public static void setOrganismName() {  
        GenomeVersion newGroup = GenometryModel.getInstance().getSelectedGenomeVersion();
        String orgName= (newGroup == null) ? "select genome" : newGroup.getSpeciesName();
        JTextField organismField = ncbiPrimerPane.getOrganismField();
        organismField.setText(orgName);          
        
         // Update Database according to the selected Species
        DefaultComboBoxModel model = new DefaultComboBoxModel(NCBIPrimerBlastModel.Database.values());
        JComboBox<String> databaseComboBox = ncbiPrimerPane.getDatabaseComboBox();
        databaseComboBox.setModel(model);

        ArrayList<String> selectedSpecies = new ArrayList<>();
        for (NCBIPrimerBlastModel.GenomeSelectedSpecies value : NCBIPrimerBlastModel.GenomeSelectedSpecies.values()) {
            selectedSpecies.add(value.toString());
        }

        if (!selectedSpecies.contains(orgName)) {
            databaseComboBox.removeItem(NCBIPrimerBlastModel.Database.genome_selected_species);
            setMenuItem();
        }
        ncbiPrimerPane.setDatabaseComboBox(databaseComboBox);
        ncbiPrimerPane.setOrganismField(organismField);
    }
}
