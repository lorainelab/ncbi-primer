/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.ncbiprimer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Pooja Nikhare
 */
/**
 * Options for running Primer-BLAST.
 *
 */
public class NCBIPrimerBlastModel {

    public NCBIPrimerBlastModel() {
        //    private static final Logger logger = LoggerFactory.getLogger(NCBIPrimerBlastModel.class);

    }

    /**
     * Database to search against for primer specificity. refseq_rna - Refseq
     * mRNA genome_selected_species - Genome (reference assembly from selected
     * organisms) ref_assembly - Genome (chromosomes from all organisms) nt -
     * non-redundant set of transcripts
     *
     */
    public enum Database {
            
        refseq_mrna,
        refseq_rna,
        refseq_representative_genomes,
        genome_selected_species,
        nt;
       
        /**
         * Convert database to the corresponding CGI parameter.
         *
         * @return database convereted to the correspoding CGI parameter
         */
        public String toCGIParameter() {
            switch (this) {
                case refseq_mrna:
                    return "refseq_mrna";
                case refseq_rna:
                    return "refseq_rna";
                case refseq_representative_genomes :
                    return "refseq_representative_genomes";
                case genome_selected_species:
                    return "PRIMERDB/genome_selected_species";
                case nt:
                    return "nt";
                default:
                    return null;
            }
        }

        /**
         * Displays the corresponding label to the database.
         *
         * @return database label
         */
        public String toString() {
            switch (this) {
                 case refseq_mrna:
                    return "Refseq mRNA";
                case refseq_rna:
                    return "Refseq RNA (refseq_rna)";
                case refseq_representative_genomes :
                    return "Refseq representative genomes";
                case genome_selected_species:
                    return "Genomes for selected organisms (primary reference assembly only)";
               case nt:
                    return "nr";
                default:
                    return null;
            }
        }
        
        public String toOrgDict(){
            switch (this) {
                 case refseq_mrna:
                    return "blast_refseq_mrna_sg";
                case refseq_rna:
                    return "blast_refseq_rna_sg";
                case refseq_representative_genomes :
                    return "blast_refseq_representative_genomes_sg";
                case genome_selected_species:
                    return "bdb_pb_sel_org_sg";
               case nt:
                    return "taxids_sg";
                default:
                    return null;
            }
        }

    }
    
    public enum GenomeSelectedSpecies {

        apis_mellifera,
        bos_taurus,
        danio_rerio,
        dog,
        drosophila_melanogaster,
        gallus_gallus,
        human,
        mouse,
        pan_troglodytes,
        pig,
        rat;

        public String toString() {
            switch (this) {
                case apis_mellifera:
                    return "Apis mellifera";
                case bos_taurus:
                    return "Bos taurus";
                case danio_rerio:
                    return "Danio rerio";
                case dog:
                    return "Canis lupus familiaris";
                case drosophila_melanogaster:
                    return "Drosophila melanogaster";
                case gallus_gallus:
                    return "Gallus gallus";
                case human:
                    return "Homo sapiens";
                case mouse:
                    return "Mus musculus";
                case pan_troglodytes:
                    return "Pan troglodytes";
                case pig:
                    return "Sus scrofa";
                case rat:
                    return "Rattus norvegicus";
                default:
                    return null;
            }
        }
    }
    
    private Integer start;
    private Integer end;
    private String primer5Start;
    private String primer5End;
    private String primer3Start;
    private String primer3End;
    private String primerLeftInput;
    private String primerRightInput;
    private Integer primerProductMin;
    private Integer primerProductMax;
    private Integer primerNumReturn;
    private Double primerMinTm;
    private Double primerOptTm;
    private Double primerMaxTm;
    private Double primerMaxDiffTm;
    private boolean searchSpecificPrimer;
    private String organism;
    private Database primerSpecificityDatabase;
    private Integer totalPrimerSpecificityMismatch;
    private Integer primer3endSpecificityMismatch;
    private Integer mismatchRegionLength;
    private Integer productSizeDeviation;
    private boolean removePairsNotInExons;
    private Integer exonJunctionMatchMin5;
    private Integer exonJunctionMatchMin3;
    private Integer exonJunctionMatchMax3;
    private Integer intronMinLengthRange;
    private Integer intronMaxLengthRange;
    private boolean intronInclusion;
    private Integer exonJunctionSpan;
    private static final Logger logger = LoggerFactory.getLogger(NCBIPrimerBlastModel.class);

    /**
     * Get the forward primer start genomic coordinate.
     *
     * @return start genomic coordinate
     */
    public String getPrimer5Start() {
        return primer5Start;
    }

    /**
     * Set the forward primer start genomic coordinate.
     *
     * @param primer5Start - start genomic coordinate
     */
    public void setPrimer5Start(String primer5Start) {
        this.primer5Start = primer5Start;
    }

    /**
     * Get the forward primer end genomic coordinate.
     *
     * @return end genomic coordinate
     */
    public String getPrimer5End() {
        return primer5End;
    }

    /**
     * Set the forward primer end genomic coordinate.
     *
     * @param primer5End - end genomic coordinate
     */
    public void setPrimer5End(String primer5End) {
        this.primer5End = primer5End;
    }

    /**
     * Get the reverse primer start genomic coordinate.
     *
     * @return start genomic coordinate
     */
    public String getPrimer3Start() {
        return primer3Start;
    }

    /**
     * Set the reverse primer start genomic coordinate.
     *
     * @param primer3Start - start genomic coordinate
     */
    public void setPrimer3Start(String primer3Start) {
        this.primer3Start = primer3Start;
    }

    /**
     * Get the reverse primer end genomic coordinate.
     *
     * @return end genomic coordinate
     */
    public String getPrimer3End() {
        return primer3End;
    }

    /**
     * Set the reverse primer end genomic coordinate.
     *
     * @param primer3End - end genomic coordinate
     */
    public void setPrimer3End(String primer3End) {
        this.primer3End = primer3End;
    }

    public String getPrimerLeftInput() {
        return primerLeftInput;
    }

    public void setPrimerLeftInput(String primerLeftInput) {
        this.primerLeftInput = primerLeftInput;
    }

    public String getPrimerRightInput() {
        return primerRightInput;
    }

    public void setPrimerRightInput(String primerRightInput) {
        this.primerRightInput = primerRightInput;
    }

    public Integer getPrimerProductMin() {
        return primerProductMin;
    }

    public void setPrimerProductMin(Integer primerProductMin) {
        this.primerProductMin = primerProductMin;
    }

    public Integer getPrimerProductMax() {
        return primerProductMax;
    }

    public void setPrimerProductMax(Integer primerProductMax) {
        this.primerProductMax = primerProductMax;
    }

    public Integer getPrimerNumReturn() {
        return primerNumReturn;
    }

    public void setPrimerNumReturn(Integer primerNumReturn) {
        this.primerNumReturn = primerNumReturn;
    }

    public Double getPrimerMinTm() {
        return primerMinTm;
    }

    public void setPrimerMinTm(Double primerMinTm) {
        this.primerMinTm = primerMinTm;
    }

    public Double getPrimerOptTm() {
        return primerOptTm;
    }

    public void setPrimerOptTm(Double primerOptTm) {
        this.primerOptTm = primerOptTm;
    }

    public Double getPrimerMaxTm() {
        return primerMaxTm;
    }

    public void setPrimerMaxTm(Double primerMaxTm) {
        this.primerMaxTm = primerMaxTm;
    }

    public Double getPrimerMaxDiffTm() {
        return primerMaxDiffTm;
    }

    public void setPrimerMaxDiffTm(Double primerMaxDiffTm) {
        this.primerMaxDiffTm = primerMaxDiffTm;
    }

    public boolean isSearchSpecificPrimer() {
        return searchSpecificPrimer;
    }

    public void setSearchSpecificPrimer(boolean searchSpecificPrimer) {
        this.searchSpecificPrimer = searchSpecificPrimer;
    }

    public String getOrganism() {
        return organism;
    }

    public void setOrganism(String organism) {
        this.organism = organism;
    }

    public Database getPrimerSpecificityDatabase() {
        return primerSpecificityDatabase;
    }

    public void setPrimerSpecificityDatabase(Database primerSpecificityDatabase) {
        this.primerSpecificityDatabase = primerSpecificityDatabase;
    }

    public Integer getTotalPrimerSpecificityMismatch() {
        return totalPrimerSpecificityMismatch;
    }

    public void setTotalPrimerSpecificityMismatch(Integer totalPrimerSpecificityMismatch) {
        this.totalPrimerSpecificityMismatch = totalPrimerSpecificityMismatch;
    }

    public Integer getPrimer3endSpecificityMismatch() {
        return primer3endSpecificityMismatch;
    }

    public void setPrimer3endSpecificityMismatch(Integer primer3endSpecificityMismatch) {
        this.primer3endSpecificityMismatch = primer3endSpecificityMismatch;
    }

    public Integer getMismatchRegionLength() {
        return mismatchRegionLength;
    }

    public void setMismatchRegionLength(Integer mismatchRegionLength) {
        this.mismatchRegionLength = mismatchRegionLength;
    }

    public Integer getProductSizeDeviation() {
        return productSizeDeviation;
    }

    public void setProductSizeDeviation(Integer productSizeDeviation) {
        this.productSizeDeviation = productSizeDeviation;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public boolean isRemovePairsNotInExons() {
        return removePairsNotInExons;
    }

    public void setRemovePairsNotInExons(boolean removePairsNotInExons) {
        this.removePairsNotInExons = removePairsNotInExons;
    }
    
    
    public Integer getExonJunctionMatchMin5() {
        return exonJunctionMatchMin5;
    }

    public void setExonJunctionMatchMin5(Integer exonJunctionMatchMin5) {
        this.exonJunctionMatchMin5 = exonJunctionMatchMin5;
    }

    public Integer getExonJunctionMatchMin3() {
        return exonJunctionMatchMin3;
    }

    public void setExonJunctionMatchMin3(Integer exonJunctionMatchMin3) {
        this.exonJunctionMatchMin3 = exonJunctionMatchMin3;
    }

    public Integer getExonJunctionMatchMax3() {
        return exonJunctionMatchMax3;
    }

    public void setExonJunctionMatchMax3(Integer exonJunctionMatchMax3) {
        this.exonJunctionMatchMax3 = exonJunctionMatchMax3;
    }

    public Integer getIntronMinLengthRange() {
        return intronMinLengthRange;
    }

    public void setIntronMinLengthRange(Integer intronMinLengthRange) {
        this.intronMinLengthRange = intronMinLengthRange;
    }

    public Integer getIntronMaxLengthRange() {
        return intronMaxLengthRange;
    }

    public void setIntronMaxLengthRange(Integer intronMaxLengthRange) {
        this.intronMaxLengthRange = intronMaxLengthRange;
    }
    
    public boolean isIntronInclusion() {
        return intronInclusion;
    }

    public void setIntronInclusion(boolean intronInclusion) {
        this.intronInclusion = intronInclusion;
    }
    
    public Integer getExonJunctionSpan() {
        return exonJunctionSpan;
    }

    public void setExonJunctionSpan(Integer exonJunctionSpan) {
        this.exonJunctionSpan = exonJunctionSpan;
    }
    
    /*
    * Sets Default Options to GUI of  NCBI primer Blast Options Pane
    */
    public void setDefaultOptions() {

        /*
         *Set Primer Parameters
         */
        setPrimer5Start("");
        setPrimer5End("");
        setPrimer3Start("");
        setPrimer3End("");

        setPrimerLeftInput("");
        setPrimerRightInput("");
        setPrimerProductMin(200);
        setPrimerProductMax(1000);
        setPrimerNumReturn(10);
        setPrimerMinTm(58.0);
        setPrimerOptTm(60.0);
        setPrimerMaxTm(63.0);
        setPrimerMaxDiffTm(3.0);

        /*
         *Set  Primer Pair Specificity Checking Parameters
         */
        setSearchSpecificPrimer(true);
        setOrganism("");
        setPrimerSpecificityDatabase(Database.refseq_rna);
//        setTotalPrimerSpecificityMismatch(2);
//        setPrimer3endSpecificityMismatch(2);
//        setMismatchRegionLength(5);
        setProductSizeDeviation(1000);
       
        /*
         *Set Post Processing Options
         */
        setRemovePairsNotInExons(true);
      //  logger.info("Default Options set for Primer Blast opts");
        setExonJunctionMatchMin3(4);
        setExonJunctionMatchMin5(7);
        setExonJunctionMatchMax3(8);
        setIntronInclusion(false);
        setIntronMaxLengthRange(1000000);
        setIntronMinLengthRange(1000);
        setExonJunctionSpan(0);
    }

}
